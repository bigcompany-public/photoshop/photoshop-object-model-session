import traceback
from typing import Any
from win32com.client import Dispatch, CDispatch
from pywintypes import com_error
from photoshop_object_model import PhotoshopObjectModel
from photoshop_object_model_session.extended_model.

class PhotoshopDispatch():
    _instance:CDispatch = None

    @classmethod
    def instance(cls) -> CDispatch:
        if not cls._instance:
            cls._instance = Dispatch("Photoshop.Application")
        return cls._instance

class PhotoshopSession(PhotoshopObjectModel):
    """
    This class is used to run code in photoshop using the Photoshop Object Model
    It is designed to be used as a context : 

    >>> with PhotoshopSession() as ps:
    >>>     print(ps.Application.Version)
    >>>     new_layer = ps.Application.ActiveDocument.ArtLayers.Add()
    >>>     new_layer.Name = "Hello World"
    """
    def __init__(
            self,
            error_popup:bool=True,
            error_message:str="The script execution encountered some errors",
            success_popup:bool=False,
            success_message:str="The script was successfully executed",
        ):
        super().__init__()
        self.error_popup = error_popup
        self.success_popup = success_popup
        self.error_message = error_message
        self.success_message = success_message

    def __getattribute__(self, __name: str) -> Any:
        if __name in ["error_popup", "error_message","success_popup", "success_message"]:
            return super().__getattribute__(__name)
        return getattr(PhotoshopDispatch.instance(), __name)

    def __enter__(self):
        return self
    
    def __exit__(self, _err_type, _err_value, _traceback):
        """
        Depending on whether or not the code was successfully executed, show an alert in photoshop
        """
        if _err_value:
            if not self.error_popup:
                return
            
            if isinstance(_err_value, com_error):
                ps_err_message = (eval(str(_err_value))[2][2])
            else:
                ps_err_message = str(_err_value)
            py_traceback = traceback.format_exc()
            py_traceback = py_traceback.replace("\\", "\\\\")
            py_traceback = py_traceback.replace("\n", "\\n")
            py_traceback = py_traceback.replace("\"", "\\\"")
            self.Application.DoJavaScript(f"""
                var message = File.decode("{self.error_message}");
                var err_value = File.decode("{ps_err_message}");
                var err_traceback = File.decode("{py_traceback}");
                alert(message + " :\\n" + err_value + "\\n\\n" + err_traceback, "Error", true);
            """
            )
        elif self.success_popup:
            self.Application.DoJavaScript(f"""
                var message = File.decode("{self.success_message}");
                alert(message, "Success", false);
            """
            )

if __name__ == "__main__":
    # t = PhotoshopDispatch.instance()
    # print(dir(t))
    with PhotoshopSession(success_popup=False) as ps:
        print(ps)
        print(ps.Application.ActiveDocument.Layers("CAMERA").Name)
