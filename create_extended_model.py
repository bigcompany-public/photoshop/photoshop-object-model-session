from pathlib import Path
import photoshop_object_model
import shutil

def main():
    base_model_path = Path(photoshop_object_model.__file__).parent
    dst_dir = Path(__file__).parent.joinpath("photoshop_object_model_session/extended_model")
    if dst_dir.exists():
        shutil.rmtree(dst_dir)
    dst_dir.mkdir()
    for src in base_model_path.iterdir():
        dst = dst_dir.joinpath(src.name)
        if src.name == "__pycache__":
            continue
        if src.name == "__init__.py":
            shutil.copy(src, dst)
            continue
    
        with src.open("r", encoding="utf-8", errors="ignore") as f:
            content = f.read()
        content = content.split('():')[0]

        # import original class
        class_name = content.rsplit(" ", 2)[-1]
        original_module = f"photoshop_object_model.{class_name.lower()}"
        original_class_alias = f"_{class_name}"

        # Inherit from original class
        index = content.find("class ")
        content = content[:index].strip() + f"\nfrom {original_module} import {class_name} as {original_class_alias}\n"
        
        # # Move future on top
        future = "from __future__ import annotations\n"
        content = content.replace(future, "")
        content = future + content

        # Create extended class
        content += f"""
class {class_name}({original_class_alias}):
    def test(self) -> str:
        ...
"""
        print(content)
        return
        # Write file
        with dst.open('w', encoding="utf-8", errors="ignore") as f:
            f.write(content)

        
        

if __name__ == "__main__":
    main()